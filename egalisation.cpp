// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
using namespace std;
int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrit[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrit);

   OCTET *ImgIn,*ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgOut, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

  float histo[256];
  for(int i = 0; i < 256;i++){histo[i]=0;}

  float unit = 1.0/(float)(nTaille);
  
  for (int i=0; i < nH; i++){
   for (int j=0; j < nW; j++)
     {
       int v =ImgIn[i*nW+j];
        histo[v] += unit;
     }
  }

  float histo2[256];
  for(int i = 0; i < 256;i++)
  {
    histo2[i]= histo[i] + (i>0 ? histo2[i-1] : 0);
  }

  for (int i=0; i < nH; i++){
   for (int j=0; j < nW; j++)
     {
       int v = ImgIn[i*nW+j];
       v = (int)(histo2[v] * 255);
        ImgOut[i*nW+j] = max(min(v,255),0);
     }
  }
  
   ecrire_image_pgm(cNomImgEcrit, ImgOut,  nH, nW);
  
   free(ImgIn); 

   return 1;
}
